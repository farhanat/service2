package com.demo.service2.controller;

import com.demo.service2.dto.RequestDTO;
import com.demo.service2.dto.ResponseDTO;
import com.demo.service2.dto.ServiceResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@Validated
@RestController
@RequestMapping(value = "/external/services/rest")
public class SampleServiceController {
    @PostMapping(value = "/sample-service",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<ResponseDTO> postSampleService(
            @RequestBody RequestDTO sampleservicerq) {

        log.info("Incoming request {}", sampleservicerq);

        ServiceResponseDTO responseDTO = ServiceResponseDTO.builder()
                .errorCode("0000")
                .errorMsg("Success")
                .trxId(sampleservicerq.getSampleservicerq().getTrxId())
                .build();

        ResponseDTO serviceResponse = new ResponseDTO(responseDTO);

        log.info("Outgoing response {}", serviceResponse);

        return ResponseEntity.status(HttpStatus.OK)
                .body(serviceResponse);
    }
}
